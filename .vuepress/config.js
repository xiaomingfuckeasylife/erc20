module.exports = {
  title: 'ERC20 Token Generator',
  description: 'Easily deploy Smart Contract.',
  base: '/erc20-generator/',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'twitter:card', content: 'summary_large_image' }],
    ['meta', { property: 'twitter:title', content: 'ERC20 Token Generator' }],
    ['script', { src: 'assets/js/web3.min.js' }],
  ],
  plugins: [
    [
      '@vuepress/google-analytics',
      {
        ga: 'UA-115756440-2',
      },
    ],
  ],
  defaultNetwork: 'mainnet',
  port:8090,
};
